# Internet libera e reti mesh

*Benjamin Cadon*

![rete](../../es/content/media/mesh-net.png)
### Accesso alla rete

La questione della sovranità tecnologica si manifesta con particolare intensità quando abbiamo a che fare con Internet e con la nostra capacità di accedervi liberamente per una serie di utilizzi, da semplici comunicazioni interpersonali allo scambio di dati, all’uso di applicazioni web di condivisione di mezzi e di organizzazione collettiva. In questo articolo tratteremo la problematica soprattutto dalla prospettiva della “rete”, partendo dal globale per poi considerare le iniziative su scala locale.

Innanzi tutto, possiamo parlare della storia di Internet. Internet nasce negli Stati Uniti, avviata grazie a finanziamenti militari, ampliata da universitari e appassionati di informatica prima di estendersi in tutto il pianeta… e quindi possiamo iniziare a porci domande sulla sua gestione. Dall’ultimo Incontro Globale sulla Società dell’Informazione (WSIS, World Summit on the Information Society), che ebbe luogo in Tunisia nel 2005, Internet è regolata dall’Internet Governance Forum, sotto la sigla dell’Organizzazione delle Nazioni Unite (ONU). 

Questa organizzazione mondiale non puó nascondere il fatto che, da un punto di vista tecnico, alcune istanze, al cuore della rete, sono finite sotto l’egemonia nordamericana. In particolare pensiamo all’ICANN (Internet Corporation for Assigned Names and Numbers)[^1] : una società di diritto californiana senza scopi di lucro, sotto la tutela del Dipartimento del Commercio degli Stati Uniti, che gestisce i server DNS “a grappolo” (i .net, .org, .com) e attribuisce gli indirizzi “IP”[^2]. Questi indirizzi identificano tutti i computer che sono presenti nella rete. Bisogna segnalare che varie iniziative per creare un sistema di DNS decentralizzato (DNS P2P), tra le quali quella di Peter Sunde, cofondatore di The Pirate Bay[^3], non hanno ottenuto fino a ora un’estensione significativa. Dobbiamo anche considerare la “censura dei DNS” come per esempio l’intervento dei servizi americani per interrompere le attività di Megaupload[^4], o quella del “governo attraverso la rete” come segnaló il collettivo artistico “Bureau d’etudes”.[^5]


### Perché bisogna difendere la neutralità di Internet?

Ripassiamo rapidamente alcuni trattati e tentativi internazionali, europei e nazionali (TAFTA, CETA, ACTA, SOPA, PIPA, Regolamento dell’Unione Internazionale delle Telecomunicazioni (ITU), DAVDSI in Europa, legge Sinde in Spagna, LOPSI e Hadopi in Francia etc.) che pretendono di mettere ostacoli alla neutralità della rete per poterla “filtrare”. Secondo il collettivo “La Quadrature du Net”[^6] : “la neutralità della rete é un principio fondatore di Internet che garantisce che gli operatori delle telecomunicazioni non discriminino le comunicazioni dei loro utenti, e si comportino come semplici trasmettitori di informazioni. Questo principio permette a tutti gli utenti, indipendentemente dai loro mezzi, di accedere alla stessa rete nella sua totalità”. Per molti e spesso discutibili motivi[^7], alcuni trattati e progetti di legge provano a fabbricare strumenti legali per obbligare i fornitori di accesso o di strumenti di rete, e/o i contributori, a intervenire nell’accesso a certi contenuti per poterli filtrare, e quindi discriminare.

La possibilità di accedere liberamente e pienamente a Internet puó anche vedersi influenzata da considerazioni strategico-commerciali dei provider, i quali, con le tecnologie Deep Packet Inspection (DPI), hanno la capacità di favorire certi contenuti piuttosto che altri. La DPI consiste nell’aprire tutti i pacchetti che contengono i dati scambiati con i server e gli altri utenti per valutarne il contenuto e decidere la loro spedizione rapida o, al contrario, indirizzarli verso una binario morto o un ascoltatore prescelto che controlla tutto. Gli interessi dei provider commerciali di accesso a Internet sono molteplici: permettono di pensare a offerte di accesso con varie velocità, per esempio, per limitare la quantità di banda dei servizi piú pesanti e meno redditizi (ad esempio YouTube) o per far pagare un accesso privilegiato a questi servizi con l’obiettivo di garantire la buona ricezione dei segnali televisivi che circolano ora via Internet, o la qualità dei servizi telefonici con IP[^8]. Bisogna sottolineare come queste tecnologie *DPI* sono usate anche dai fabbricanti di armi digitali per mettere sotto vigilanza un intero paese in rivolta (ad esempio la Libia, aiutata dai tecnici e dal software Eagle sviluppato dall’impresa francese Ameys Bull[^9]).

### La neutralità di Internet, un principio da difendere da un punto di vista tecno-politico

Alcuni stati puntano, ancora molto timidamente, a garantire un accesso libero e completo a Internet. Dopo il Cile[^10] é il caso, per esempio, dei Paesi Bassi dove il parlamento ha adottato una legge sulla neutralità di Internet all’inizio del mese di Maggio 2012[^11], mentre l’Unione Europea continua a sorvolare sul tema[^12]. In alcuni paesi, le amministrazioni pubbliche hanno la possibilità giuridica di assumere il ruolo di Internet provider e proporre un servizio di qualità a prezzi piú bassi per le fasce di popolazione meno avvantaggiate (ad esempio la Régie Communale du Câble et d’Electricité de Montataire in Francia[^13]) o che si trovano in zone in cui non arrivano i provider commerciali perché poco profittevoli (le “zone bianche”). Fino a ora, almeno in Francia, le amministrazioni sono state piú rapide a delegare l'impianto delle reti di banda larga ai soliti operatori commerciali piuttosto che ad approfittare di questa opportunità per affrontare concretamente il futuro di Internet sotto il punto di vista dei beni comuni.

Alcuni attori della società civile da molto tempo si sono mobilizzati per difendere questo principio di fronte ai legislatori, come nel caso della “Quadrature du Net” che lo ha convertito in una delle sue priorità[^14] e si presenta come una “organizzazione di difesa dei diritti e delle libertà dei cittadini di Internet. Promuove un’adattamento della legislazione francese ed europea che si mantenga fedele ai valori che hanno promosso lo sviluppo di Internet, soprattutto la libera circolazione della conoscenza. In questo senso, la Quadrature du Net interviene nei dibattiti sulla libertà di espressione, il diritto d’autore, le regolamentazioni del settore delle telecomunicazioni e anche il rispetto della vita privata. Consegna ai cittadini interessati strumenti che permettano loro di comprendere meglio i processi legislativi e partecipare efficacemente al dibattito pubblico” [^15].

### Comunità per una Internet accessibile, libera e aperta

Esistono varie tipologie di associazioni, ONG e comunità che militano in forma attiva e (in maniera) pratica per proporre una rete neutrale. Si possono distinguere da un punto di vista tecnico secondo il modo di accesso proposto: dal dotarsi di un router per connettersi a una rete cablata o, ancora meglio, all’installare un sistema wifi integrato in una rete mesh a sua volta interconnessa a Internet. In linguaggio tecnico, “Asymmetric digital Subscriber Line” (linea di abbonamento digitale asimmetrica, ADSL) contro il Wi-Fi, una banda libera dello spettro elettromagnetico.

### Linea di abbonamento digitale asimmetrica

Possiamo citare come esempio francese la French Data Network (FDN)[^16], creata nel 1992 come associazione per offrire a tutti e a minor prezzo quello che altri usavano come strumento di lavoro dall’inizio degli anni ottanta. I servizi offerti da FDN hanno incluso la posta elettronica, le notizie, l’accesso a numerosi archivi di software e documentazione e alle macchine della rete Internet.

Uno dei vantaggi di FDN è la diversità dei suoi membri, con vecchi navigatori di Internet ben preparati tecnicamente e membri interessati a temi molto differenti (musica, legge, educazione, grafica etc.). Questo permette di promuovere una Internet di qualità, sia a livelli di servizi che di contenuti che ne rispettano l’etica iniziale. Partendo da questa volontà, FDN ha avviato in Francia una federazione di provider associati per l’accesso a Internet (FFDN), che al momento comprende 23 membri[^17] e che cerca di facilitare lo scambio su problematiche tecniche e politiche. 
La creazione di un FAI  (“ fournisseur d’accès a Internet “: fornitore di accesso a Internet)  associativo[^18] sembra relativamente sensata (vedere “come diventare il proprio FAI[^19]”[^20]) soprattutto quando strutture come la FFDN si profilano per accompagnare e dinamizzare questa iniziativa. Ci rimane il problema del “circuito locale”, ovvero gli ultimi chilometri del cavo, e un domani della fibra ottica, che arrivano fino alla nostra casa, e che appartengono a un numero limitato di operatori con i quali bisogna giungere a un accordo. Una problematica dalla quale restano esenti le reti wireless.


### Il Wi-Fi, una banda libera dello spettro elettromagnetico

Con il cambiamento della legislazione, all’inizio del 2000, in alcuni paesi, si rendeva possibile liberamente l’utilizzo di apparati di comunicazione wireless, senza dover chiedere nessun tipo di autorizzazione o licenza. Molti paesi limitarono la potenza ammessa e aprirono piú o meno “canali” in una banda di radiofrequenza che si chiama “Industriale, Scientifica e Medica” (ISM[^21]) che si trova tra i 2.4 e i 2.4835 Ghz. Al tempo stesso, in alcuni paesi, esiste la possibilità di usare frequenze attorno ai 5Ghz. 

A partire da qui, si sono iniziate a creare comunità Wi-Fi, tanto nelle città per essere piú autonomi, mutualisti e liberi rispetto ai fornitori di accesso, cosí come nelle campagne per coprire “zone bianche” senza connessione a Internet e considerate come “non profittevoli” per gli operatori privati e pubblici. Bisogna menzionare in Europa: Freifunk[^22] in Germania, Funkefeuer[^23] in Austria o Guifi.net[^24] in Catalogna, o Ninux in Italia tra molte altre[^25]. Sono molto eterogenee, includendo da pochi utenti in zone isolate fino a decine di migliaia di “nodi” distribuiti in zone piú dense, su scala cittadina, regionale o nazionale.

In maniera schematica, i membri costituiscono un punto di accesso e un ripetitore dentro una rete mesh configurando un router Wi-Fi in maniera adeguata. Questa rete si connette a Internet tramite uno o piú accessi personali o condivisi: un’antenna fa da collegamento con zone distanti svariati chilometri dove un’altra piccola rete puó essere sviluppata. Si tratta quindi di distribuire nella maniera piú decentralizzata possibile l’accesso a Internet e a risorse informatiche “locali” (siti web, servizi di posta elettronica, strumenti di telecomunicazione etc.), cioé preposte in uno dei server direttamenti connessi a uno o piú nodi di questo intreccio elettromagnetico. 

Una delle piú antiche comunità Wi-Fi in Europa, Freifunk (“radio libera”), nata nel 2002, creó un proprio sistema operativo per routers, il Firmware Freifunk, e il proprio protocollo di routing B.A.T.M.A.N.[^26], oggi in uso su scala mondiale come base per costruire reti mesh e ottimizzare in queste la circolazione di pacchetti. Fece anche parte della costituzione di una rete internazionale di comunità che condividono gli stessi valori, spesso vicini a quelli del software libero, con la stessa voglia di distribuire, “decentralizzare”, nella dimensione possibile, i mezzi della rete che si considerano come un bene comune al quale tutti possano accedere.

L’abbassamento dei prezzi dei router Wi-Fi (fatti nella Repubblica Popolare Cinese[^27]) aiutó lo sviluppo di questa tipologia di iniziativa che alcuni vedono come il futuro di Internet: una rete decentralizzata, stabile, con una intelligenza multiforme e condivisa, che si adatta a tutto quello che puó succedere socio-tecno-ecologicamente in ogni contesto. Sicuramente esistono rivendicazioni a proposito della questione della “liberazione delle frequenze”[^28], perché anche agli operatori privati fanno comodo queste onde “gratuite”, sia per poter far comunicare tra di loro oggetti suppostamente intelligenti, sia per trasmettere la telefonia mobile attravero il cavo di Internet di casa; alcune ora chiamano questa banda di frequenza la “banda bassa”. Ora quindi possiamo considerare questa risorsa elettromagnetica come un bene comune, mettendo la società al centro del processo di scambio, ben oltre l’influenza degli Stati e delle aziende sulle frequenze. Organismi come “Wireless commons” stabilirono un manifesto e una lista di punti comuni che potessero caratterizzare queste organizzazioni, e il fondatore di Guifi.net pubblica dal 2005 il Comun Sesefils[^29] (Licenza Comune Wireless).

### Artisthackers sperimentano con altre “reti”

Presentiamo alcune iniziative che contribuiscono alla problematica della sovranità tecnologica dalla questione dell’accesso a un sistema di comunicazione e di scambio aperto, accessibile e anonimo:


### Workshops sull'autogestione informatica

Negli hackerspace e in altri medialab, o per dirlo in altro modo, nei luoghi di riappropriazione della tecnologia, si realizzano workshop, piú o meno regolarmente, per essere piú autonomi di fronte alle proprie necessità informatiche: come avere i propri server web/mail in casa; come cifrare le proprie comunicazioni; aggirare possibili sistemi di filtraggio e schivare, per quanto possibile, gli ascoltatori indesiderati; come gestire i propri dati personali, la sicurezza del computer, etc...


### Battle mesh

In questo stesso tipo di luoghi, si organizzano “wireless battle mesh”[^30], riunioni amatoriali di specialisti in comunicazioni di reti wireless, che nel corso di diversi giorni e sotto forma di un gioco, di una battaglia, testano vari protocolli e provano a ottimizzare il funzionamento di una rete mesh per aquisire esperienze e abilità, interfacciandosi con altri partecipanti che condividono queste problematiche tecniche.


### “Qaul.net” di Christoph Wachter e Mathias Jud

Qaul.net implementa un principio di comunicazione aperta nella quale computer e apparati mobili equipaggiati di scheda Wi-Fi possono formare in maniera spontanea una rete tra di loro, e permettere lo scambio di testi, files e chiamate vocali senza dover passare attraverso Internet o una rete di telefonia mobile. Questo progetto “artistico” è stato immaginato in reazione ai “blackout” di comunicazione imposti da regimi oggetto di rivolte all’interno del paese, o nel caso in cui catastrofi naturali distruggano le infrastrutture di rete.


### “Batphone” o “Serval Mesh”

L’obiettivo di questo progetto è di trasformare ogni telefono cellulare con il Wi-Fi in un telefono Wi-Fi, cioè in un mezzo di comunicazione che, appoggiandosi alle strutture wireless esistenti, permetta la comunicazione con altre persone all’interno della rete senza passare dalla casella dell’operatore e senza aver bisogno di una scheda SIM.[^31]


### “Deaddrop” di Aram Barthol

Il progetto consiste nel cementare in una parete una chiave USB e condividere la sua localizzazione in una mappa apposita lanciata sulla rete dall’artista[^32]. Si tratta di un’appropriazione della cassetta delle lettere usata da generazioni di spie per comunicazioni senza contatto fisico. Si tratta di un modo di creare un luogo di scambio anonimo, da persona a persona, disconnesso da Internet e impiantato nello spazio pubblico. I “deaddrops” si sono diffusi in (quasi) tutto il pianeta e dichiarano di avere al momento 7144Gb di spazio di archiviazione. Incidentalmente possono prendere freddo o riempirsi di virus.

### “Piratebox” di David Darts

La Piratebox[^33] ripropone questo stesso princio di cassa di deposito anonima proponendo una rete Wi-Fi aperta nella quale tutte le persone che si connettono e aprono un browser Internet si vedono diretti verso una pagina che propone il caricamento dei propri file, e la consultazione e scaricamento dei file depositati precedentemente. Questa “micro-Internet” è disconnessa dalla grande Internet, non registra i “logs” e garantisce, quindi, confidenzialità. Si puó accedere al sistema in una radio che ha a che vedere con il posizionamento e la qualità dell’antenna utilizzata, si puó installare in un router Wi-Fi a basso costo come il micro computer Raspberry Pi e aggiungerle una chiave Wi-Fi, o in un computer tradizionale, o in un telefono cellulare. 
Partendo da questo dispositivo, la comunità degli utilizzatori ha immaginato molte evoluzioni[^34] : la “Library Box” per condividere libri liberi dal diritto d’autore in una biblioteca, il “Micro Cloud” per tenere i documenti a portata di mano, la “OpenStreetMap Box” per consultare risorse cartografiche libere “offline”, la T.A.Z. Box, la Pedago-Box, la KoKoBox, etc.


#### Conclusioni

Tra quello che è in gioco a livello internazionale e le disuguaglianze locali, è possibile che sia conveniente tenere a mente uno dei principi fondatori di Internet, “distribuire l’intelligenza”. Si rende necessario evitare la centralizzazione tecnica e decisionale per rivolgerci, invece, verso uno scambio aperto delle conoscenze e dei dispositivi tecnici, e la difesa collettiva dell’idea per cui Internet sia un bene comune al quale si deve poter accedere liberamente. Possiamo immaginare che ogni mattina, ognuno potrà cercare Internet nella casa della sua artigiana di reti locale, come le verdure succulente che coltiva con amore un produttore appassionato. Internet non deve essere la scatola nera chiusa poco a poco da un ristretto numero di persone, ma deve essere considerata come un oggetto tecnico di cui appropriarsi, del quale è necessario mantenere il controllo, che va coltivato collettivamente nella sua diversità e affinché ci nutra con degli ottimi bytes.


------


*Benjamin Cadon*

Artista e coordinatore di [Labomedia](http://labomedia.org), mediahackerfablabspace senza scopo di lucro basato a Orléans, Francia.

**benjamin[at]labomedia[dot]org**


----


###### NOTE

[^1]: https://it.wikipedia.org/wiki/ICANN

[^2]: Un indirizzo IP chiamato “pubblico” è quello che permette a un computer di essere parte di Internet e di parlare lo stesso linguaggio ,il protocollo TCP/IP, per scambiare dati con suoi affini: server, personal computers, terminali mobili e altri oggetti chiamati “comunicanti”. I server DNS servono a trasformare questi indirizzi IP in nomi di dominio affinché i server sian piú accessibili agli umani e ai robot dei motori di ricerca.

[^3]: http://www.bortzmeyer.org/dns-p2p.html "Un DNS in peer-to-peer?" - Stéphane Bortzmeyer

[^4]: http://torrentfreak.com/megaupload-shut-down-120119 MegaUpload Shut Down by the Feds, Founder Arrested

[^5]: http://bureaudetudes.org/2003/01/19/net-governement-2003/

[^6]: http://www.laquadrature.net

[^7]: Con "motivi discutibili" ci riferiamo al fatto di nascondere le offensive contro la neutralità della rete sotto il pretesto di voler proteggere la proprietà intellettuale e il diritto d’autore, prevenire il terrorismo e l’aumentare degli estremismi, o anche la lotta contro la pedofilia e altri comportamenti predatori nella rete. Non diciamo che questi problemi non esistono, ma provare a risolverli attraverso restrizioni della libertà in rete, dove la neutralità è un principio base, è un errore fondamentale.

[^8]: https://es.wikipedia.org/wiki/VOIP Wikipedia VOIP

[^9]: http://reflets.info/amesys-et-la-surveillance-de-masse-du-fantasme-a-la-dure-realite/

[^10]: http://www.camara.cl/prensa/noticias_detalle.aspx?prmid=38191

[^11]: http://www.numerama.com/magazine/22544-la-neutralite-du-net-devient-une-obligation-legale-aux-pays-bas.html

[^12]: http://www.laquadrature.net/fr/les-regulateurs-europeens-des-telecoms-sonnent-lalarme-sur-la-neutralite-du-net. Sulla neutralita' della rete vedere anche la campagna: http://savetheinternet.eu/fr/

[^13]: http://www.rccem.fr/tpl/accueil.php?docid=2

[^14]: http://www.laquadrature.net/fr/neutralite_du_Net

[^15]: http://www.laquadrature.net/fr/qui-sommes-nous

[^16]: http://www.fdn.fr/

[^17]: http://www.ffdn.org/fr/membres

[^18]: http://www.ffdn.org/fr/article/2014-01-03/federer-les-fai-participatifs-du-monde-entier Una mappa dell’evoluzione dei FAI

[^19]: http://blog.spyou.org/wordpress-mu/2010/08/19/comment-devenir-son-propre-fai-9-cas-concret/

[^20]: http://blog.spyou.org/wordpress-mu/?s=%22comment+devenir+son+propre+fai%22

[^21]: https://es.wikipedia.org/wiki/Banda_ISM Banda ISM, vedere anche https://fr.wikipedia.org/wiki/Bande_industrielle,_scientifique_et_m%C3%A9dicale

[^22]: http://freifunk.net/

[^23]: http://www.funkfeuer.at/

[^24]: http://guifi.net/

[^25]: https://en.wikipedia.org/wiki/List_of_wireless_community_networks_by_region Lista reti wireless comunitarie, vedere anche http://ninux.org 

[^26]: http://www.open-mesh.org/projects/open-mesh/wiki

[^27]: Si veda il contributo di Elleflane su “Hardware Libero” in questo dossier.

[^28]: http://owni.fr/2011/05/07/le-spectre-de-nos-libertes/ Allegato di Félix Treguer e Jean Cattan a favore della liberazione delle frequenze « Le spectre de nos libertés » lo spettro delle nostre libertà

[^29]: https://guifi.net/ca/CXOLN

[^30]: http://www.battlemesh.org/

[^31]: https://github.com/servalproject/batphone

[^32]: http://deaddrops.com/dead-drops/db-map/

[^33]: http://daviddarts.com/piratebox/?id=PirateBox

[^34]: http://wiki.labomedia.org/index.php/PirateBox#Projets_et_d.C3.A9tournements_de_la_PirateBox

<p align="center"><img src="../../end0.png"></p>
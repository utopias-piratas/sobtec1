#Criptomonedas

##Jorge Timón


“Si le pregunta a un economista ‘¿qué es el dinero?’ probablemente este no le responda con una definición sino con una enumeración de las tres funciones atribuidas tradicionalmente al dinero. A saber: medio de intercambio, unidad de valor y almacén de valor”. Citando a Bernard Lietaer1 y dejando de lado si el “buen” dinero, si es que existe, debe proporcionar todas esas funciones o si es siquiera posible, las opiniones sobre qué es exactamente suelen divergir bastante.


Algunos lo consideran un activo económico como otro cualquiera, mientras otros niegan que sea capital real (al no ser directamente un medio de producción) ni que sea tampoco un bien de consumo al no desaparecer cuando circula de mano en mano. Unos lo consideran un acuerdo social (implícito o impuesto explícitamente por un estado) y otros simplemente una tecnología para el intercambio de bienes y servicios.
Si nos preguntamos sobre su historia, una explicación comúnmente aceptada es que el oro se alzó como dinero al ser la materia más fácilmente comercializable en trueque. El antropólogo David Graeber2 niega la existencia de pruebas y propone las economías de regalos3/dones y monedas basadas en crédito mutuo4 como los orígenes más probables del comercio.
Los reformistas monetarios5 ven en la estructura del dinero la raíz de muchos de los problemas de nuestras sociedades. De hecho, hoy en día existen y circulan más monedas complementarias/locales/sociales que monedas oficiales6. Ya en plena crisis del 29 el alcalde de la localidad tirolesa de Wörgl7 decidió poner en práctica la teoría de la libre moneda de Silvio Gesell8. A pesar de su éxito, el banco central austríaco interrumpió el experimento e impidió que las localidades vecinas copiasen el modelo.
Por su parte, el movimiento Cypherpunk9 originado en la década de los 80, aboga por el uso extendido de la criptografía como herramienta de cambio social y político. En 1990, David Chaum lanzaba Digicash10, un sistema centralizado de dinero electrónico que permitía transacciones más anónimas y seguras. En 1997, Adam Black proponía Hashcash11, un sistema basado en la prueba de trabajo para limitar el spam (correos no deseados) y los ataques de denegación de servicio (DoS). En 2009, una identidad desconocida bajo el seudónimo de Satoshi Nakamoto publicaba Bitcoin12, la primera criptomoneda completamente descentralizada, utilizando una cadena de bloques con prueba de trabajo, de la que se hablará en más detalle.


Desde su aparición, muchas otras criptomonedas basadas o inspiradas en ella han surgido pero es importante destacar que no todas son monedas p2p13 descentralizadas. Algunas se crearon para añadir alguna funcionalidad adicional14, por diferencias ideológicas en lo económico15, para tratar de solucionar problemas técnicos16; aunque la mayoría se limitan a pequeños cambios sin importancia o se crean por puro afán especulativo o de fraude17. En cualquier caso, un requisito indispensable para ser una moneda p2p es que el sistema se base en software libre18, pues de otro modo estaría bajo el control de sus desarrolladores y las personas usuarias no podrían confiar en él.

**Principales agentes**

***Hackers y otros entusiastas***

En un principio los únicos que utilizaban Bitcoin eran informáticos, entusiastas de la criptografía o del software libre. Una práctica habitual ha sido por ejemplo la de recaudar recompensas para pagar a programadores, generalmente para que implementen algun desarrollo bajo software libre relacionado con las propias monedas. Otros grupos que rápidamente se sintieron atraídos por las similitudes entre el oro como dinero y el bitcoin fueron los seguidores de la escuela austríaca19 (la corriente económica dominante en la comunidad de monedas p2p) y los anarco-capitalistas20.

***Los “mineros”*** 

Estos ponen su hardware a disposición de la red p2p y realizan la prueba de trabajo (Proof of Work- POW) en la que se basa la seguridad de la mayoría de estas criptomonedas. Aunque algunos mineros hayan conseguido amasar una fortuna debido en parte a la suerte y a las grandes fluctuaciones a la alza en el precio de las monedas, la minería se ha convertido en un negocio muy competitivo, complejo y arriesgado donde es relativamente fácil perder dinero, ya sea por los costes de electricidad o por la incapacidad de recuperar la inversión inicial.

***Empresas, cooperativas, colectivos especializados***

Muchas empresas han surgido alrededor de estas tecnologías para cubrir nichos de mercados como por ejemplo: mercados para intercambiar criptomonedas entre sí o por monedas oficiales, empresas que procesan pagos eliminando los riesgos de volatilidad para los comerciantes, carteras web, anuncios por bitcoin, micro-donativos, etc. Hay que notar que muchas son sólo adaptaciones de modelos de negocios que ya existían para el entorno de las monedas p2p. Pero muchas otras están también propiciando innovación en un sector tan regulado y controlado por carteles como el financiero.

***Especuladores***

Hay quien se dedica al arbitraje entre los distintos mercados existentes y en realidad puede cumplir una función importante. Sin embargo el tipo más frecuente de especulador es el que simplemente se dedica a atesorar monedas p2p con la esperanza de que su precio suba. Por si el bitcoin no fuese ya suficiente volátil de por sí, estos especuladores disfrutan ahora de una gran variedad de nuevas monedas con mercados más pequeños (y con ello, por lo general, más volátiles), en los que poder seguir arriesgando hasta el extremo.

***Productores y comerciantes***
Estos pueden fidelizar o conseguir clientes adicionales aceptando criptomonedas. Corren riesgos derivados de la fluctuación del precio de las monedas (aunque existen servicios para cancelarlos), pero gozan de comisiones más baratas e irreversibilidad de las transacciones. En comparación, gran parte de las comisiones con tarjetas de crédito o servicios como paypal se justifica por el alto nivel de fraude debido a que los pagos pueden ser cancelados posteriormente.

***Ciudadanía y Organizaciones sin ánimo de lucro***

Recibir donativos en monedas p2p siempre ha sido extremadamente sencillo, basta con poner una dirección o código QR en una página web, o en una pancarta21. Algunas organizaciones sin ánimo de lucro pioneras en aceptar bitcoins han recibido importantes cantidades, que a menudo se han vuelto mucho más valiosas con la posterior apreciación de la moneda. Por otra parte, organisaciones del tercer sector también están desarrollando proyectos y experimentando en este terreno. Por ejemplo, el 90% de la generación de Devcoin22 se destina a proyectos de conocimiento libre, aunque la toma de decisiones sean centralizadas. O aún Freicoin quien entrega el 80% de la cantidad inicial emitida en 3 años a la Fundación Freicoin para que los distribuya utilizando métodos experimentales de distribución aceptados y desarrollados previamente por la comunidad . De momento sólo hay un programa de emisión que consiste en una plataforma de crowdfunding23 para organizaciones y proyectos sin ánimo de lucro24: cualquiera puede donarles freicoins, y la fundación aporta un 10% extra, sin tener que elegir directamente cuánto dinero entrega a cada una. Cualquiera puede auditar la cadena de bloques de transacciones para comprobar que el reparto se ha realizado de la forma esperada.

**Censurados y bloqueados**

Otra ventaja fundamental es la imposibilidad de llevar a cabo censura. Por un lado, los pagos pueden venir desde cualquier parte del mundo. Sólo Paypal bloquea a más de 60 países y muchas compañías de tarjetas tienen restricciones similares. Organizaciones como Wikileaks también han sido bloqueadas por Visa, Mastercard y Paypal impidiéndoles recibir donaciones en monedas oficiales pero sí pudieron recibirlas en monedas p2p.
Paradójicamente cuanto más pobre es un país, más altas son las comisiones e intereses que se les cobran. Es frecuente que el total de lo que un país paga en comisiones a entidades financieras extranjeras supere el total de ayudas que recibe. Los inmigrantes que envían dinero a su país también suelen pagar comisiones obscenas superiores al 10%, muy poco competitivas en comparación con las comisiones fijas marcadas por monedas p2p (a menudo inferiores a un céntimo de euro). Además, en muchos países, gran parte de la población adulta no tienen acceso a ningún tipo de servicio financiero, ni cuenta corriente. En Kenya, el 31% del producto interior bruto se transfiere mediante teléfonos móviles usando el sistema m-pesa25. un ejemplo de empresas relacionadas con las monedas p2p26 .


***Problemáticas y limitaciones***

***Macroeconomía***
Sólo resumiremos de forma escueta las principales posiciones entorno a la “calidad” de las criptomonedas como dinero en sentido macroeconómico. La escuela austríaca suele recibir bien la proposición de una cantidad fija de dinero máximo o creación predecible. Los neokeynesianos27, más abundantes e influyentes, no encuentran su lugar entre las criptomonedas ya que opinan que a veces la economía “necesita más dinero”.
Otra corriente más minoritaria e ignorada es la iniciada por Silvio Gesell, según la cuál el problema no es una falta de dinero sino su estancamiento. Cuando los rendimientos de capital y los intereses son bajos los ahorradores simplemente dejan de invertir y prestar el dinero. Freicoin28 por ejemplo aplica una comisión de oxidación29 para evitar su estancamiento y suprimir la ventaja del prestamista para poder negociar el interés más a la alta.

**El problema de la emisión**

Aunque es necesario compensar a los mineros por la seguridad que proporcionan, en el futuro debería ser suficiente con las comisiones por transacción. En general la distribución inicial de las criptomonedas es un asunto controvertido sobre el que seguro se seguirá experimentando, y que nos hace también reflexionar sobre la creación del propio dinero oficial. Hay quien piensa30 que no deberían llevarla a cabo los bancos comerciales y centrales, sino que el señoreaje31 debería recibirlo el estado.

**Hardware especializado**

Otro asunto es el de los circuitos integrados de aplicación específica (ASICs)32. Se trata de hardware especializado para una tarea concreta, en este caso, la minería. El argumento en contra de las ASICs suele ser el de la centralización, temiendo que pudiese surgir un monopolio o una gran concentración en su producción y/o distribución. Pero aunque fuera posible escapar de ellas para siempre, no todo el mundo piensa que sean algo a evitar33, argumentando que esa centralización existía cuando la forma más eficiente de minar era usando GPUs (tarjetas gráficas), pues el mercado está controlado prácticamente por dos compañías y en la práctica la mayoría de los mineros compraban a la misma (ATI).

**Pools y centralización**

Las pools son un grupo organizado de mineros que apuestan juntos para repartirse la recompensa de los bloques que consigan dependiendo del poder de computación que haya aportado cada uno. El problema es que sólo el operador de la pool valida el bloque en el que contribuyen a ciegas el resto de participantes. El operador podría abusar de este poder para atacar el sistema sin que sus mineros se diesen cuenta y también podría engañarlos.

**Privacidad**

Se leen muchos comentarios en internet sobre como la supuesta anonimidad de Bitcoin la convierte en la moneda preferida por los criminales. Pero la realidad es que a lo largo de toda su historia, todas las transacciones han sido públicas y cualquiera puede descargar la cadena de bloques para verlas alejándola del ideal tipo de moneda anónima.
Aunque tampoco es un sistema diseñado para la vigilancia orwelliana de las finanzas, ya que cualquiera puede crear cualquier número de claves donde recibir pagos y no tener su nombre directamente asociado a las direcciones (seudónimos). A menos, por supuesto, que el dueño se lo diga a las personas que quiera o las publique en internet (o si sus conexiones en internet estan siendo vigiladas34). Algunos proyectos como coinjoin35 o darkwallet36 están orientados a mejorar la privacidad de los usuarios sin modificar el protocolo básico de bitcoin. Otros proyectos como zerocoin37 optan por modificarlo (crear una nueva criptomoneda) para ofrecer más anonimidad, aunque ello pueda suponer menos eficiencia u otros efectos no deseados.

**Escalabilidad**

Uno de los desafíos más importante al que se enfrentan estas monedas a largo plazo radica en su capacidad38 para crecer en número de transacciones procesadas. VISA, por ejemplo, procesa una media de 2000 transacciones por segundo (tps) y podría procesar hasta 10000 tps. En contraste, Bitcoin solo puede procesar hasta 7 tps, aunque algunos de los límites que imponen ese máximo sean artificiales. Existe un compromiso delicado entre escalabilidad y centralización, pues con muchas transacciones, menos gente operará nodos completos (en contraste con clientes ligeros39).

**Conclusiones**


Es probable que a corto y medio plazo las criptomonedas sigan siendo muy volátiles. Igual que uno puede ganar dinero rápidamente especulando con su valor, también lo puede perder, por lo que no es sensato especular con grandes cantidades. Además, hay que tener especial cuidado con las más nuevas, pues a menudo se trata de proyectos con comunidades pequeñas que solo pueden proporcionar un mantenimiento limitado al software.
Las organizaciones y proyectos sin ánimo de lucro, sin embargo, no corren riesgos por aceptar donaciones en estas monedas, es algo relativamente sencillo de hacer y les pueden proporcionar una fuente adicional de ingresos. Para los freelancers puede resultar una herramienta muy útil para poder ser contratado desde cualquier parte del mundo, pero como cualquier otro comerciante o productor, es responsable de venderlas pronto por monedas oficiales y/o en un porcentaje suficiente para no sufrir de los riesgos asociados a su volatilidad.
Sea cual sea el destino de cada moneda independientemente, la tecnología ofrece ventajas suficientes como para esperar que algunas de ellas (u otras que estén por crear) encuentren su lugar en la sociedad para mantenerse a largo plazo. En cierto sentido, su potencial disruptivo para la industria monetario-financiera es comparable al que tecnologías p2p como bittorrent40 han causado a la industria del copyright. Es improbable, sin embargo, debido a ciertas limitaciones, que estas monedas sean las únicas, siendo más realista pensar que convivirán con las monedas oficiales y la tendencia también creciente de otro tipo de monedas complementarias (locales, sociales, entre negocios B2B41 etc).

**Jorge Timón** 
Ingeniero informático con más de 4 años de experiencia en Indra, trabajando en varios proyectos internacionales incluyendo programas para varias compañías aseguradoras grandes. Ha contribuido al diseño  del protocolo distribuido Ripple (anterior a Ripple Labs) desarrollado por Ryan Fugger. Propuso y codiseñó Freicoin. Es el principal desarrollador de la página de la Fundación Freicoin. Ha sido ponente en la segunda conferencia internacional sobre monedas complementarias y en Bitcoin Europa 2013 entre otras conferencias.

---

**NOTE**

1. http://en.wikipedia.org/wiki/Bernard_Lietaer
2.	 http://en.wikipedia.org/wiki/Debt:_The_First_5000_Years 
3. http://en.wikipedia.org/wiki/Gift_economy
4. http://en.wikipedia.org/wiki/Mutual_credit
5. http://en.wikipedia.org/wiki/Monetary_reform  
6. http://www.complementarycurrency.org/ccDatabase/ 
7.http://en.wikipedia.org/wiki/Worgl#The_W.C3.B6rgl_Experiment 
8. http://en.wikipedia.org/wiki/Silvio_Gesell 
9. http://en.wikipedia.org/wiki/Cypherpunk

<p align="center"><img src="../../end0.png"></p>
**Prefazione**
===
**La sovranità tecnologica, una necessità, una sfida.**
---
***Patrice Riemens***

Tutte ormai abbiamo capito, dopo “Snowden” e le sue rivelazioni, che il nostro caro “cyberspazio” non è gestito da chi lo usa. E questo, sfortunatamente, da molto tempo, al punto che oggi si tratta di una zona molto vigilata e rischiosa. L’utente, apparentemente libera nei suoi movimenti e dotata di innumerevoli facilitazioni -spesso fornite “gratuitamente”- si è convertita di fatto in un soggetto prigioniero che è, allo stesso tempo, ostaggio, cavia da laboratorio e sospettato. 

Il dominio su Internet dei poteri statali o commerciali, o, molto spesso, una combinazione dei due, sembra totale, ed effettivamente lo è dove i vettori e le piattaforme sono “proprietarie”, cioè quando sono in possesso di certi attori che possono mettere in prima linea i propri interessi, frequentemente a costo degli interessi delle loro utenti. Mentre l’impatto che Internet ha sulle nostre vite si fa sempre più forte[^1], una presa di coscienza a proposito del come e, soprattutto, del per chi funziona, è sempre più urgente.

Fortunatamente, questa presa di coscienza esiste ed è iniziata molto prima della diffusione di Internet. Tuttavia, i suoi effetti rimangono limitati, perché per ora interessano solo un numero relativamente ristretto di persone e gruppi; e anche perché si scontrano con forti offensive da parte di alcuni dei poteri stabiliti più potenti. Suo portabandiera è il software libero, e i suoi numerosi derivati. Non solo come tecnica, ma anche soprattutto per l’ideale che rappresenta: presa di coscienza, presa con le proprie mani -autonomia e sovranità. Perché attenzione, non tutto è tecnologia e la tecnologia non è tutto. 

È necessario percepire la sovranità tecnologica in un contesto più esteso della tecnologia informatica, o più inclusivo rispetto la sola tecnologia informatica. Ignorare la congiuntura di crisi ambientali, politiche, economiche e sociali intersecate l’una nell’altra[^2], o cercare di risolverle in forma isolata o nella loro sola congiunzione tecnologica sono opzioni ugualmente perverse. Sembra ormai cristallino che la sola sovranità tecnologica non cambierà il nostro inesorabile cammino… contro un muro. 

È impossibile continuare con la via della crescita su tutti i livelli, così come è stata seguita fino a ora. Una fermata in più è necessaria, forse anche una decrescita volontaria. In ogni caso succederà lo stesso, e in condizioni sicuramente meno piacevoli. Inoltre, da questa prospettiva, dovremmo considerare le differenti soluzioni proposte per (ri)conquistare questa autonomia individuale e collettiva che abbiamo perso ampliamente o, ancora peggio, delegata a beneficio di attori economici e politici che vogliono farci credere che pensano solo ai nostri interessi e che le loro intenzioni sono benevole, oneste e legittime.

Sfortunatamente le Tecnologie di Informazione e Comunicazione (TIC) e i suoi sviluppatori -perché sono ancora per la maggioranza uomini- hanno la tendenza a lavorare isolati, senza tenere conto della loro dipendenza con la moltitudine di relazioni umane e risorse naturali che compongono il mondo e la società. “Dobbiamo reinventare la rete”, ha dichiarato Tim Pritlove, animatore del trentesimo Congresso del Chaos Computer Club, nel suo discorso di apertura [^3] nel 2013. Per aggiungere davanti a una moltitudine di attivisti e hacker entusiasti: "*e siete voi coloro che possono farlo*". Ha ragione su due fronti, ma soffermarsi qui potrebbe anche significare la credenza in una “supremazia dei nerd” [^4] che metteranno tutto a posto tramite soluzioni puramente tecnologiche. 

Non c’è alcun dubbio sul fatto che sia diventato essenziale ricomporre la rete dalla base affinché serva agli interessi comuni, e non solo agli interessi di gruppi esclusivi e oppressori. Quindi, sì al reinventarsi, ma non a prescindere. Perché è necessario andare più in là di soluzioni del tipo “technological fix” (“pezze tenologiche”) che si limitano ad attaccare gli effetti senza toccare le cause. Un approccio dialettico -e dialogico- è necessario per sviluppare una base comunitaria e partecipativa, le tecnologie che permettono, a chi le usa, di liberarsi dalla loro dipendenza dai provider commerciali, e dal monitoraggio poliziesco generalizzato da parte dei poteri statali annebbiati dal loro desiderio di vigilare e castigare. Ma quindi, in cosa consiste questa sovranità tecnologica desiderata e che vogliamo costruire? 

Un’opzione è quella di iniziare il nostro approccio partendo dalla sovranità che si manifesta nella nostra sfera di vita personale, in rispetto ai poteri che provano a dominarci. Un principio di sovranità potrebbe essere interpretato per esempio come “il diritto a essere lasciate tranquille [^5]". Senza dubbio, sappiamo che questo diritto si vede sempre calpestato nel campo delle (“nuove”) tecnologie d’informazione e di comunicazione. 

Questo dossier prova a stabilire una valutazione della situazione relativa alle iniziative, ai metodi e ai mezzi non-proprietari e preferibilmente autogestiti che possono salvaguardare al meglio la nostra “sfera di vita”. Server autonomi, reti decentralizzate, crittografia, peer to peer, monete alternative virtuali, la condivisione del sapere, luoghi di incontro e lavoro cooperativo, si costituiscono come un gran ventaglio di iniziative già in marcia verso la sovranità tecnologica. Si osserva che l’efficacia di queste alternative dipende in gran misura dalla loro pratica (e pratiche) e queste dovrebbero essere attraversate dalle seguenti dimensioni:

**Temporalità**

“Prendersi il tempo” è essenziale. Dobbiamo liberarci del sempre di più, sempre più rapido: il canto delle sirene della tecnologia commerciale. Accettare che le tecnologie “sovrane” siano più lente e offrano meno prestazioni, ma non per questo ci sará una perdita del nostro piacere. 

**Nostre**

Le tecnologie “sovrane” dovranno essere aperte, partecipative, egualitarie, comunitarie e cooperative, o non saranno. 
Sviluppare meccanismi di governo orizzontale coinvolgendo spesso gruppi molto differenti. La separazione, le gerarchie (spesso presentate come “meritocrazia”) e l’individualismo egoista le uccidono. La distinzione tra “esperte” e “utenti” deve sparire nella misura del possibile.

**Responsabilità**

La realizzazione della sovranità esige molto da parte di coloro che si affiliano ad essa. Sviluppando e dispiegando i suoi strumenti, ogni membro del collettivo deve prendere le sue responsabilità. È necessario applicare la famosa norma:
“Chi fa cosa? Dove? Quando? Come? Quanto? E Perché?” con l’obbligo di rispondere in ogni momento a tutte queste domande.

**Un’economia basata sullo scambio**

Il principio del “se è gratuito, allora tu sei il prodotto” caratterizza i servizi *regalati* dai pesi massimi di Internet. Le iniziative cittadine si vedono, abitualmente, spinte verso “l’economia del dono”, sotto forme di volontariato più o meno forzate. Bisognerà incontrare quindi nuovi modelli che paghino, in maniera onesta, le “lavoratrici dell’immateriale” facendo pagare il giusto prezzo alle utenti.

**Ecologia e ambiente**

Una tecnologia di sovranità è, evidentemente, rispettosa dell’ambiente e riduce al massimo le risorse poco sostenibili e non rinnovabili. Poche persone si accorgono di quanto l’informatica divori energia e materie prime diverse, e le condizioni, spesso deplorevoli, in cui queste ultime sono estratte o in cui vengono assemblate. 

Seguendo questo percorso capiremo che esistono numerosi limiti contro i quali le tecnologie sovrane devono combattere, e che non esiste un cammino dorato per arrivare ad esse. E anche se ci arrivassimo, potrebbe non essere l’utopia. Questo, senza dubbio, non è un invito ad abbassare le braccia: al contrario. La modestia e la lucidità, unite alla riflessione, muovono montagne. Siete voi, care lettrici e cari lettori, che dovete iniziare a muovere le vostre braccia per definire il vostro contributo ed essere coinvolte senza ingenuità e senza paura. E chi può dirlo, se sarà con un entusiasmo indistruttibile e contagioso.

---

**Patrice Riemens**

Geografo, attivista culturale, diffusore del software libero, membro del collettivo di hacker olandesi “Hippies from Hell”.

---

**NOTE**

[^1]: www.faz.net/aktuell/feuilleton/debatten/abschied-von-der-utopie-die-digitale-kraenkung-des-menschen-12747258.html Come scriveva recentemente il saggista tedesco Sascha Lbo “ci sono solo due persone in Germania: quelli che hanno visto la loro vita cambiare con Internet e quelli che non si sono accorti che la loro vita è cambiata con Internet”

[^2]: Quello che il filosofo francese Paul Virilio chiama “l’incidente integrale”

[^3]: http://www.securitytube.net/video/9246

[^4]: http://es.wikipedia.org/wiki/Nerd

[^5]: Negli Stati Uniti, questo concetto del “diritto a essere lasciati da soli” è concepito come il fondamento del diritto alla privacy individuale (ver Warren & Brandeis, 1890). Fonte: http://en.wikipedia.org/wiki/The_Right_to_Privacy_%28article%29. Ma attenzione, questa sovranità nella propria sfera di vita, teorizzata quasi nello stesso momento anche nei Paesi Bassi dal politico Abraham Kuyper, ha avuto anche una gran brutta applicazione : l’Apartheid sudafricano... 

[^6]: Fonte: http://fr.wikipedia.org/wiki/QQOQCCP 

<p align="center"><img src="../../end0.png"></p>